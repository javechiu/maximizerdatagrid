﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataTable.aspx.cs" Inherits="DataTableSandBox.Views.TableSandBox.DataTable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="Scripts/pageScroll.js"></script>
    
    <link href="Content/align.css" rel="stylesheet" type="text/css"/>
    <!-- DataTable -->
    <link href="./Lib/DataTable/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="./Lib/DataTable/1.10.12/js/jquery.dataTables.min.js"></script>
    <link href="./Lib/DataTable/select/1.2.0/css/select.dataTables.min.css" rel="stylesheet"/>
    <script type="text/javascript" src="./Lib/DataTable/select/1.2.0/js/dataTables.select.min.js"></script>

    <%--<script type="text/javascript" src="Scripts/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="Scripts/dataTables.select.min.js"></script>--%>
    <script type="text/javascript" src="Scripts/MaximizerDataGrid.js"></script>
    <%--<script type="text/javascript" src="Scripts/resize.js"></script>--%>
    <%--<link href="Content/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="Content/select.dataTables.min.css" rel="stylesheet" type="text/css"/>--%>
    <link href="Content/MaximizerDataGrid.css" rel="stylesheet" type="text/css"/>

    <script>
        setTableName("myTable");
        //$(document).ready(function () {
        function genTable() {
            var tableElement = document.createElement("table");
            tableElement.id = getTableName();
            for (var j = 1; j <= 1; j++) {
                var theadElement = document.createElement("thead");
                var rowElement = document.createElement("tr");
                for (var i = 1; i <= 5; i++) {
                    var dataElement = document.createElement("th");
                    var divElement = document.createElement("div");
                    var nodeElement = document.createTextNode("Header " + i);
                    divElement.appendChild(nodeElement);
                    dataElement.appendChild(divElement);
                    rowElement.appendChild(dataElement);
                }
                theadElement.appendChild(rowElement);
                tableElement.appendChild(theadElement);

            }
            var tbodyElement = document.createElement("tbody");
            for (var j = 1; j <= 5; j++) {
                var rowElement = document.createElement("tr");
                for (var i = 1; i <= 5; i++) {
                    var dataElement = document.createElement("td");
                    var nodeElement = document.createTextNode("Row "+j+" Column " + i);
                    dataElement.appendChild(nodeElement);
                    rowElement.appendChild(dataElement);
                }
                tbodyElement.appendChild(rowElement);

            }
            tableElement.appendChild(tbodyElement);
            document.getElementById("tableContainer").appendChild(tableElement);
        }
        function formatTable() {
            var sort = localStorage.getItem(getTableName()+"_sort");    //stored sort setting
            var table;
            var pageHorizonScroll = getVerticalScroll();
            var collength = $('#' + getTableName()).find('th').length;
            var columns = [collength];
            for (var i = 0; i < collength; i++) {
                columns[i] = {};
            }
            if (sort != null) {
                sort = sort.split(",");
                if (sort[0] < 0) sort[0] = 0;
                table = $('#'+getTableName()).DataTable({
                    "aoColumns": columns,
                    order: [[Number(sort[0]), sort[1]]],
                    autoWidth: false,
                    bAutoWidth: false,
                    dom: 'rt',
                    scrollY: pageHorizonScroll,
                    scrollCollapse: true,
                    paging: false,
                    select: {
                        style: 'single'
                    },
                    keys: {
                        keys: [38 /* up */, 40 /* down */, ],
                    },
                    bSortClasses: false
                });
            }
            else {
                table = $('#' + getTableName()).DataTable({
                    "aoColumns": columns,
                    autoWidth: false,
                    bAutoWidth: false,
                    dom: 'rt',
                    scrollY: pageHorizonScroll,
                    scrollCollapse: true,
                    paging: false,
                    select: {
                        style: 'single'
                    },
                    keys: {
                        keys: [38 /* up */, 40 /* down */, ],
                    },
                    bSortClasses: false
                });
            }

            setting();
            //select row
            table.on('select', function (e, dt, type, indexes) {
            })
            //deselect row
            .on('deselect', function (e, dt, type, indexes) {
            });
            //scroll table
            $(".dataTables_scrollBody").on("scroll", function (e) {
                scrolling(e);
            });
            //select cell
            $(document).on("click", ".dataTables_scrollBody .cellSpan", function (e) {
                var cell = $(".dataTables_scrollBody .cellSpan").index($(this));
                return true;
            });
            //right click menu
            $('#tableContainer').contextmenu(function () {
                //add your code here
                return true;    //true for enable context menu, false for disable
            });
            ///resize window
            $(window).resize(function (e) {
                resizing(e);
            });
        }
    </script>
    <title>DataTable Sand Box</title>
</head>
<body>
    <form id="form1">
    <textarea></textarea>
    <div id="tableContainer">
<%--        <table id="myTable" class="resizable">
          <thead>
            <tr class="colHeaders">
              <th class="colHeader"><div>Column 1</div></th>
              <th class="colHeader"><div>Column 2</div></th>
              <th class="colHeader"><div>Column 3</div></th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <td class="alignLeft">Row 1, col 1</td>
            <td class="alignCenter">Row 1, col 2</td>
            <td class="alignRight">Row 1, col 3</td>
          </tr>
          <tr>
            <td class="col">Row 2, col 1</td>
            <td class="col">Row 2, col 2</td>
            <td class="col">Row 2, col 3</td>
          </tr>
          <tr>
            <td class="col">Row 3, col 1</td>
            <td class="col">Row 3, col 2</td>
            <td class="col">Row 3, col 3</td>
          </tr>
          <tr>
            <td class="col">Row 4, col 1</td>
            <td class="col">Row 4, col 2</td>
            <td class="col">Row 4, col 3</td>
          </tr>
          <tr>
            <td class="col">Row 5, col 1</td>
            <td class="col">Row 5, col 2</td>
            <td class="col">Row 5, col 3</td>
          </tr>
          <tr>
            <td class="col">Row 6, col 1</td>
            <td class="col">Row 6, col 2</td>
            <td class="col">Row 6, col 3</td>
          </tr>
          </tbody>
        </table>--%>
    </div>
        
        <input type="button" onclick="getColumnWidth();" value="get column"/>
        <input type="button" onclick="getColSort();" value="get header"/>
        <input type="button" onclick="getTableWidth();" value="get table"/>
        <input type="button" onclick="getSelectRow();" value="get select row"/>
        <input type="button" onclick="removeSession();" value="remove session"/>
        <input type="button" onclick="genTable();" value="generate table"/>
        <input type="button" onclick="formatTable();" value="format table"/>
    </form>
</body>
</html>
