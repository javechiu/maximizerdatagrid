﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Scroll.aspx.cs" Inherits="DataTableSandBox.TableSandBox.Scroll" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
<head runat="server">
    <title></title>
    <script>
        $(document).ready(function () {
            $(".dataTables_scrollBody").on("scroll", function (e) {
                $('#result').append("Scrolled! ");
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="dataTables_scrollBody" style="overflow:auto;height:100px;width:100px;">
            clear 
            Search results
            Sign in

            Sign in to get your bookmarks, history, passwords and other settings on all your devices. You'll also automatically be signed in to your Google services. Learn more
            Sign in to Chrome
            clear
            Privacy

            Content settings… 
            clear
            Clear browsing data…
            Google Chrome may use web services to improve your browsing experience. You may optionally disable these services. Learn more


            Use a web service to help resolve navigation errors
 
            Use a prediction service to help complete searches and URLs typed in the address bar
 
            Use a prediction service to load pages more quickly
 
            Automatically send some system information and page content to Google to help detect dangerous apps and sites
 
            Protect you and your device from dangerous sites
 
            Use a web service to help resolve spelling errors
 
            Automatically send usage statistics and crash reports to Google
 
            Send a "Do Not Track" request with your browsing traffic
            Downloads

            Download location:  
            C:\Users\jave\Downloads
 
         </div>
        <div id="result"></div>
    </div>
    </form>
</body>
</html>
