﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataTable.aspx.cs" Inherits="DataTableSandBox.Views.TableSandBox.DataTable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <script type="text/javascript" src="~/Content/jquery.dataTables.min.js"></script>
    <link href="~/Scripts/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
    <title>DataTable Sand Box</title>
</head>
<body>
    <form id="form1">
    <div>
        <table id="myTable" class="resizable">
          <caption>Data Table with resizable columns</caption>
          <thead>
            <tr class="colHeaders">
              <th>Column 1</th>
              <th>Column 2</th>
              <th>Column 3</th>
            </tr>
          </thead>
          <tbody>
          <tr>
            <td>Row 1, col 1</td>
            <td>Row 1, col 2</td>
            <td>Row 1, col 3</td>
          </tr>
          </tbody>
        </table>
    </div>
    </form>
</body>
</html>
