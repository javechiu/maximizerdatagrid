﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DataTableSandBox.Startup))]
namespace DataTableSandBox
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
