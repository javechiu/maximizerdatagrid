﻿function GetZoomFactor() {
    var factor = 1;
    if (document.body.getBoundingClientRect) {
        // rect is only in physical pixel size in IE before version 8 
        var rect = document.body.getBoundingClientRect();
        var physicalW = rect.right - rect.left;
        var logicalW = document.body.offsetWidth;

        // the zoom level is always an integer percent value
        factor = Math.round((physicalW / logicalW) * 100) / 100;
    }
    return factor;
}
function getHorizonScroll() {
    if ('pageXOffset' in window) {	// all browsers, except IE before version 9
        var scrollLeft = window.pageXOffset;
    }
    else {		// Internet Explorer before version 9
        var zoomFactor = GetZoomFactor();
        var scrollLeft = Math.round(document.documentElement.scrollLeft / zoomFactor);
    }
    return scrollLeft;
}
function getVerticalScroll() {
    if ('pageXOffset' in window) {	// all browsers, except IE before version 9
        var scrollTop = window.pageYOffset;
    }
    else {		// Internet Explorer before version 9
        var zoomFactor = GetZoomFactor();
        var scrollTop = Math.round(document.documentElement.scrollTop / zoomFactor);
    }
    return scrollTop;
}