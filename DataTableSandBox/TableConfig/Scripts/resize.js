﻿$(function () {
    var pressed = false;
    var resizer = undefined;
    var header = undefined;
    var content = undefined;
    var startX, columnWidth, startWidth;
    var tableWidth;
    var padding = 20;
    var identifier;
    var datatable = $('#myTable');
    $(".colResizer").mousedown(function (e) {

        resizer = $(this);
        var identifier = resizer.attr('id').substring(10);

        header = $('.dataTables_scrollHead').find('th:nth-child('+(Number(identifier)+1)+')');
        content = datatable.find('th:nth-child(' + (Number(identifier) + 1) + ')');

        pressed = true;
        startX = e.pageX;

        columnWidth = header.width();
        tableWidth = $('.dataTables_scrollHeadInner').find('.dataTable').width();

        $(resizer).addClass("resizing");
    });

    $(document).mousemove(function (e) {
        if (pressed) {
            var h_padding = 18;
            var plusWidth = e.pageX - startX;

            //$(resizer).offset({ left: e.pageX });

            $('.dataTables_scrollHeadInner').find('.dataTable').css('width', tableWidth + plusWidth + 'px');
            datatable.css('width', tableWidth + e.pageX - startX + 'px');

            header.css('width', columnWidth + plusWidth + 'px');
            content.css('width', columnWidth + plusWidth + 'px');

            var headerCount = datatable.find('th').length;
            for (var i = 0; i < headerCount; i++) {
                var currentHeader = $('.dataTables_scrollHead').find('.colHeader:nth-child(' + (Number(i) + 1) + ')');
                $('#colResizer' + i).offset({ left: currentHeader.position().left + currentHeader.width() + h_padding * 2 + 3 });
            }

            $('.dataTable_scrollHeader').attr("overflow", "visible");
        }
    });

    $(document).mouseup(function () {
        if (pressed) {
            $(resizer).removeClass("resizing");
            pressed = false;
        }
    });
});