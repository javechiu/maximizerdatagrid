﻿var containerWidth;
var containerHeight;
var resizerX = new Array;
var marginLeft;
var applicationName = "default";
//arrange table
function tuneTable() {
    //$(".dataTables_scrollBody").height("100%");
    //$(".dataTable").width("100%");
    //$(".dataTables_scrollHeadInner").width("100%");
    $(".dataTables_scrollHead").css("overflow", "hidden");
    $(".dataTables_scrollHeadInner").css("overflow", "visible");
    //$(".dataTables_scrollBody").css("overflow", "visible");
    $(".dataTables_scrollBody table").css("margin-left", "0px");

    //append resizer to document
    var lastCol = $('#tableContentRightPad');
    if (lastCol.length > 0) {
        lastCol.empty();
        lastCol.width("auto");
    }
    else
        $('.dataTables_scrollBody th:last-child').append('<span id="tableContentRightPad" >');
    setColBySession();
    setResizer();
    tablePadding();
}
//set up the table, initialization process
function setting() {
    //table fit into'#tableContainer'. if content is overflow, scroll bar appear

    $('dataTables_scrollHeadInner table').css("border-bottom", "1px sold #ddd");

    var container = $('#tableContainer');
    containerWidth = container.width();
    containerHeight = container.height();
    $('.dataTables_scrollHead').width(Number(containerWidth) + 'px');

    $('.dataTables_scrollBody').width(Number(containerWidth) + 'px');
    $('.dataTables_scrollBody').height(Number(containerHeight) - $('.dataTables_scrollHead').height() + 'px');

    //pack table content cell into <div><span>...</span></div>
    var tableDatas = $('.dataTables_scrollBody td');
    var tableData;
    var innerElement;
    var content;
    var colAmount = $('.dataTables_scrollBody th').length;
    for(var i = 0;i < tableDatas.length;i++){
        tableData = tableDatas[i];
        content = tableData.innerHTML;
        tableData.innerHTML = "";
        //$(tableData).append(   "<div style='overflow:hidden;'>"+
        //                    "<span class='cellSpan' id='col" + (Math.floor((i+1) / colAmount)+1) + "_" + (i+1) % (colAmount+1) + "'>" +
        //                    content +
        //                    "</span>"+
        //                    "</div>");
        $(tableData).append("<div style='overflow:hidden;'>" +
                           "<span class='cellSpan'>" +
                           content +
                           "</span>" +
                           "</div>");
    }
    tuneTable();

    //load select row once only
    var selectRow = localStorage.getItem(applicationName + "_selectRow");
    if (selectRow == null)
        return
    var row = $('.dataTables_scrollBody').find('tbody tr:nth-child(' + (Number(selectRow) + 1) + ')');
    row.find('td').click();

    $('#resizer_grp').width(Number(containerWidth) + 'px');
}
var localstor;
//set width of columns by session
function setColBySession() {
    if (typeof (Storage) !== "undefined") {
        localstor = true;
    } else {
        console.log("Sorry, your browser does not support Web Storage...");
        localstor = false;
        return;
    }
    var colWidth = localStorage.getItem(applicationName+"_columnWidth");
    var tabWidth = localStorage.getItem(applicationName+"_tableWidth");
    if (colWidth == null || tabWidth == null)
        return;
    colWidth = colWidth.split(",");
    var header, content;
    var headers = $('.dataTables_scrollHead').find('th');
    //resize all columns width
    for (var i = 0; i < headers.length; i++) {
        header = $('.dataTables_scrollHead').find('th:nth-child(' + (Number(i) + 1) + ')');
        content = $('.dataTables_scrollBody').find('th:nth-child(' + (Number(i) + 1) + ')');
        header.css('width', colWidth[i] + 'px');
        content.css('width', colWidth[i] + 'px');
        $('.dataTables_scroll tr th:nth-child(' + (Number(i) + 1) + ') div').css('width', colWidth[i] + 'px');
        $('.dataTables_scroll tr td:nth-child(' + (Number(i) + 1) + ') div').css('width', colWidth[i] + 'px');
    }
    //resize table width
    $(".dataTable").width(tabWidth + "px");
    $(".dataTables_scrollBody").find('table').width(tabWidth + "px");
}
//update session data of specific column
function setSessionCol(num) {
    if (typeof (Storage) !== "undefined") {
        localstor = true;
    } else {
        console.log("Sorry, your browser does not support Web Storage...");
        localstor = false;
        return;
    }
    //store column width
    var colWidth = localStorage.getItem(applicationName+"_columnWidth");
    var mLeft = document.getElementsByClassName('dataTables_scrollBody')[0].scrollLeft;
    if (colWidth == null)
        return;
    colWidth = colWidth.split(",");
    colWidth[num] = $('.dataTables_scrollHead').find('th:nth-child(' + (Number(num) + 1) + ')').width();
    localStorage.setItem(applicationName+"_columnWidth", colWidth);
    //store table width
    localStorage.setItem(applicationName+"_tableWidth", $('.dataTable').width());

    ////store column width
    //var colWidth = localStorage.getItem("columnWidth");
    //if (colWidth == null)
    //    return;
    //colWidth = colWidth.split(",");
    //colWidth[num] = resizerLeft[i];
    //localStorage.setItem(applicationName+"_columnWidth", colWidth);
    ////store table width
    //localStorage.setItem(applicationName+"_tableWidth", $('.dataTable').width());

}
//arrange resizer to the right of the header
function setResizer() {
    var header, content;
    var headers = $('.dataTables_scrollHead').find('th');
    var h_padding = 10;
    var v_padding = 3.5;
    if (typeof (Storage) !== "undefined") {
        localstor = true;
    } else {
        console.log("Sorry, your browser does not support Web Storage...");
        localstor = false;
    }

    //append resizer to document
    var dt = $('.dataTables_scroll');
    if ($('#resizer_grp').length == 0){
        dt.append('<div id="resizer_grp" >');
        for (var i = 0; i < headers.length; i++) {
            $('#resizer_grp').append('<span id="colResizer' + i + '" class="colResizer"></span>');
        }
    }
    var contentStart = new Array(headers.length);
    var tempAry;
    for (var i = 0; i < headers.length; i++) {

        marginLeft = Number($('.dataTables_scrollHeadInner').css("margin-left").replace('px', ''));
        header = $('.dataTables_scrollHead').find('th:nth-child(' + (Number(i) + 1) + ')');
        content = $('.dataTables_scrollBody').find('th:nth-child(' + (Number(i) + 1) + ')');
        resizer = $("#colResizer" + i);

        //0:left;1:top
        //var hPosition = [Number(header.css('left').replace('px', '')), Number(header.css('top').replace('px', ''))];

        resizerX[i] = header.position().left + header.width() + h_padding * 2 - 10 - marginLeft;
        resizer.css("left", Number($('#tableContainer').position().left + resizerX[i]) + 'px');
        resizer.css("top", Number(header.position().top) + 'px');
        resizer.css("margin-left", marginLeft + 'px');
        if (resizerX[i] > containerWidth - marginLeft - 10)
            resizer.css("display", "none");
        //resizer.offset({ top: $('#tableContainer').position().top + header.position().top });
        contentStart[i] = content.width();
    }
    if (localstor) {
        localStorage.setItem(applicationName+"_columnWidth", contentStart);
        localStorage.setItem(applicationName+"_tableWidth", $('.dataTable').width());
    }
}
function getColumnWidth() {
    alert(localStorage.getItem(applicationName+"_columnWidth"));
}
function getColSort() {
    alert(localStorage.getItem(applicationName+"_sort"));
}
function getSelectRow() {
    alert(localStorage.getItem(applicationName+"_selectRow"));
}
function getTableWidth() {
    alert(localStorage.getItem(applicationName+"_tableWidth"));
}
function removeSession() {
    localStorage.removeItem(applicationName + "_columnWidth");
    localStorage.removeItem(applicationName + "_tableWidth");
    localStorage.removeItem(applicationName + "_sort");
    localStorage.removeItem(applicationName + "_selectRow");
}
//fit table to container when table is smaller than container
function tablePadding() {
    if (containerWidth > resizerX[resizerX.length-1]) {
    //if (containerWidth > $('.dataTables_scrollBody table').width()) {
        $('.dataTables_scrollBody th:last-child').width($('.dataTables_scrollBody th:last-child').width() + containerWidth - $('.dataTables_scrollBody table').width());

        $('#tableContentRightPad').width(containerWidth - $('.dataTables_scrollBody table').width());
        $('.dataTables_scrollBody table').css("width", "auto");
    }
    else
        $('#tableContentRightPad').width(0);

}
function setApplicationName(name) {
    applicationName = name;
    datatable = $('#' + name);

}
function getApplicationName() {
    return applicationName;
}
function scrolling(e) {
    var mLeft = document.getElementsByClassName('dataTables_scrollBody')[0].scrollLeft;
    $('.dataTables_scrollHeadInner').css('margin-left', -1 * mLeft + 'px');
    $('.colResizer').css('margin-left', -1 * mLeft + 'px');
    for (var i = resizerX.length - 1; 0 <= i; i--) {
        if (resizerX[i] > containerWidth - 10) {
            if (resizerX[i] <= mLeft + containerWidth - 10)
                $('#colResizer' + i).css("display", "inline");
            else
                $('#colResizer' + i).css("display", "none");
        } else {
            break;
        }
    }
    marginLeft = Number($('.dataTables_scrollHeadInner').css("margin-left").replace('px', ''));
}
function resizing(e) {
    tuneTable();
    $('.colResizer').css('margin-left', marginLeft + 'px');
}
function clickheader(e){
    //store sorting and the number of column
    if (typeof (Storage) !== undefined) {
        var order = $('.dataTables_scrollHeadInner th').index(event.currentTarget);
        var sort = $(event.currentTarget).attr('aria-sort');
        if (sort == "ascending")
            sort = "desc";
        else if (sort == "descending")
            sort = "asc"
        else if (sort == undefined)
            sort = "asc"          //default
        localStorage.setItem(applicationName+"_sort", order + ',' + sort);
    }
    setTimeout(function () {
        tuneTable();
        var ind = $('.dataTables_scrollBody tr').index($('.dataTables_scrollBody .selected'));
        if (ind > -1) {
            localStorage.setItem(applicationName + "_selectRow", ind-1);
        }
    }, 0);
}
$(function () {
    var datatable;
    //resizer variable
    var pressed = false;                    //check "resizing" 
    var resizer = undefined;                //resizer in resizing
    var header = undefined;                 //table header in resizing
    var content = undefined;                //table data in resizing
    var startX;                             //left of the resizer before resizing
    var columnWidth;                        //width of the resize column
    var tableWidth;                         //width of the table
    var identifier;
    //if click header, rearrange table
    datatable.find('thead th').bind("click.MyDT", function (event) {
        clickheader(event);
    });
    $(document).on("mousedown", ".colResizer", function (e) {

        resizer = $(this);
        identifier = resizer.attr('id').substring(10);

        header = $('.dataTables_scrollHead').find('th:nth-child(' + (Number(identifier) + 1) + ')');
        content = datatable.find('th:nth-child(' + (Number(identifier) + 1) + ')');

        pressed = true;
        startX = e.pageX;

        columnWidth = header.width();
        tableWidth = $('.dataTables_scrollHeadInner').find('.dataTable').width();

        $(resizer).addClass("resizing");
    });
    ///resize column - move after clicking resizer
    $(document).mousemove(function (e) {
        if (pressed) {
            var h_padding = 10;
            var minColWidth = 15;
            var plusWidth = e.pageX - startX;

            //set minimum width of column
            if (columnWidth + plusWidth < minColWidth)
                return;

            $('.dataTables_scrollHeadInner').find('.dataTable').css('width', tableWidth + plusWidth + 'px');
            datatable.css('width', tableWidth + plusWidth + 'px');

            header.css('width', columnWidth + plusWidth + 'px');
            content.css('width', columnWidth + plusWidth + 'px');

            $('.dataTables_scroll tr th:nth-child(' + (Number(identifier) + 1) + ') div').css('width', columnWidth + plusWidth + 'px');
            $('.dataTables_scroll tr td:nth-child(' + (Number(identifier) + 1) + ') div').css('width', columnWidth + plusWidth + 'px');

            var headerCount = datatable.find('th').length;
            for (var i = 0; i < headerCount; i++) {
                var currentHeader = $('.dataTables_scrollHead').find('.colHeader:nth-child(' + (Number(i) + 1) + ')');
                var chPositionLeft = Number(currentHeader.css('left').replace('px', ''));
                resizerX[i] = currentHeader.position().left + currentHeader.width() + h_padding * 2 - 10 - marginLeft;
                if (resizerX[i] > containerWidth - marginLeft-10)
                    $('#colResizer'+i).css("display","none");
                else
                    $('#colResizer' + i).css("display", "inline");
                //$('#colResizer' + i).offset({ left: $('#tableContainer').position().left + resizerX[i] });
                $('#colResizer' + i).css("left", Number($('#tableContainer').position().left + resizerX[i]) + 'px');
            }
            //$('.dataTable_scrollHeader').attr("overflow", "visible");
        }
    });
    ///resize column - release click
    $(document).mouseup(function () {
        if (pressed) {
            $(resizer).removeClass("resizing");
            pressed = false;
            tablePadding();
            for (var i = identifier; i < resizerX.length;i++)
                setSessionCol(i);
        }
    });
    ///store select/deselect setting
    $(document).on("click", '.dataTables_scrollBody tr', function (e) {
        if (typeof (Storage) == "undefined")
            return;
        var checkselect = $(this).attr('class');
        if (checkselect.indexOf("selected") >= 0) {
            var row = $('.dataTables_scrollBody tbody tr').index($(this));
            localStorage.setItem(applicationName+"_selectRow", row);
        } else {
            localStorage.removeItem(applicationName + "_selectRow");
        }
    });
    ///disable right click
    $('#tableContainer').contextmenu(function () {
        //add your code here
        return true;    //true for enable context menu, false for disable
    })
    //cell click event
});
